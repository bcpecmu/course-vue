import axios from 'axios'
const apiUrl = 'http://localhost:8081'
export async function hello() {
  const { data } = await axios.get(`${apiUrl}`)
  console.log(data)
}
export default {
  hello,
}
